unit Workers;
{-------------------------------} 
interface 

type
  date = record 
    year: integer; 
    month: integer; 
    day: integer; 
  end;

type
  employee = record 
    name: string; 
    sname: string; 
    DateOfEmployment: date; 
  end;

type
  mass = array [1..1000] of employee;


procedure generatoin(var massiv: mass; size: integer);
function SearchMaxData(massiv: mass; size: integer): date;
function SearchMinData(massiv: mass; size: integer): date;
function FindingWorkExperience(massiv: mass; data: date; size: integer): real;
procedure PrintMass(massiv: mass; size: integer);
{-------------------------------} 
implementation 

procedure generatoin(var massiv: mass; size: integer);
var
  p, i: integer;
begin
  randomize;
  for i := 1 to size do
  begin
    p := random(10);
    case p of
      0: massiv[i].name := 'Ekaterina';
      1: massiv[i].name := 'Alexander';
      2: massiv[i].name := 'Evgnia';
      3: massiv[i].name := 'Aleksey';
      4: massiv[i].name := 'Mihail';
      5: massiv[i].name := 'Denis';
      6: massiv[i].name := 'Svetlana';
      7: massiv[i].name := 'Andrey';
      8: massiv[i].name := 'Ivan';
      9: massiv[i].name := 'Sergey';
    end;
    p := random(11);
    case p of
      0: massiv[i].sname := 'Fabrikantova';
      1: massiv[i].sname := 'Churakov';
      2: massiv[i].sname := 'Akimova';
      3: massiv[i].sname := 'Frolov';
      4: massiv[i].sname := 'Shurkin';
      5: massiv[i].sname := 'Demkin';
      6: massiv[i].sname := 'Churakova';
      7: massiv[i].sname := 'Elcov';
      8: massiv[i].sname := 'Golubev';
      9: massiv[i].sname := 'Lomtev';
      10: massiv[i].sname := 'Filchakov';
    end;
    p := random(6) + 2010;
    massiv[i].DateOfEmployment.year := p;
    
    if  massiv[i].DateOfEmployment.year = 2015 then p := random(7) + 1
    else p := random(12) + 1;
    massiv[i].DateOfEmployment.month := p; 
    
    if  ((massiv[i].DateOfEmployment.month = 7) and  (massiv[i].DateOfEmployment.year = 2015)) 
      then p := random(16)
    else p := random(31) + 1;
    massiv[i].DateOfEmployment.day := p;
  end;
end;

function SearchMaxData(massiv: mass; size: integer): date;
var
  i: integer;
  max: date;

begin
  
  max.day := 0;
  max.month := 0;
  max.year := 0;
  for i := 1 to size do
  begin
    if massiv[i].DateOfEmployment.year > max.year then
      max := massiv[i].DateOfEmployment
    else if massiv[i].DateOfEmployment.year = max.year then
      if massiv[i].DateOfEmployment.month > max.month then
        max := massiv[i].DateOfEmployment
      else if massiv[i].DateOfEmployment.month = max.month then
        if massiv[i].DateOfEmployment.day > max.day then
          max := massiv[i].DateOfEmployment;
    
  end;
  SearchMaxData := max; 
end;

function SearchMinData(massiv: mass; size: integer): date;
var
  i: integer;
  min: date;

begin
  
  min.day := 32;
  min.month := 13;
  min.year := 2019;
  
  for i := 1 to size do
  begin
    if massiv[i].DateOfEmployment.year < min.year then
      min := massiv[i].DateOfEmployment
    else if massiv[i].DateOfEmployment.year = min.year then
      if massiv[i].DateOfEmployment.month < min.month then
        min := massiv[i].DateOfEmployment
      else if massiv[i].DateOfEmployment.month = min.month then
        if massiv[i].DateOfEmployment.day < min.day then
          min := massiv[i].DateOfEmployment;
    
  end;
  SearchMinData := min;
end;

function FindingWorkExperience(massiv: mass; data: date; size: integer): real;
var
  i, CommonWorkExperience, WorkExperience: integer;

begin
  for i := 1 to size do
  begin
   
   //
   
   if massiv[i].DateOfEmployment.day <= data.day then
    begin
      if massiv[i].DateOfEmployment.month <= data.month then 
      begin
        WorkExperience := data.year-massiv[i].DateOfEmployment.year ; 
      end 
      else if massiv[i].DateOfEmployment.month >= data.month then 
        WorkExperience := data.year-massiv[i].DateOfEmployment.year  -1; 
    end 
    else if massiv[i].DateOfEmployment.month <= data.month then 
    begin
      WorkExperience := data.year-massiv[i].DateOfEmployment.year ;  
    end 
    else WorkExperience := data.year-massiv[i].DateOfEmployment.year  -1; 
    
   // 
    
    if WorkExperience > 0 then
      CommonWorkExperience := CommonWorkExperience + WorkExperience;
  end;
  FindingWorkExperience := CommonWorkExperience / size;
  
end;

procedure PrintMass(massiv: mass; size: integer);
var
  i: integer;
begin
  for i := 1 to size do
  begin
    writeln(massiv[i].name);
    writeln(massiv[i].sname);
    writeln(massiv[i].DateOfEmployment.year, '-', massiv[i].DateOfEmployment.month, '-', massiv[i].DateOfEmployment.day);
    writeln();
  end;
end;
{---------------------------------} 

begin 

end. 